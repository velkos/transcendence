import {deleteValue} from '../utilities/array/deleteValue.mjs';
import {Capsule} from '../encapsulation/Capsule.mjs';

const EVENTS = Capsule.createKey();

export class EventEmitter {
	constructor() {
		Capsule.set({
			owner: this,
			key: EVENTS,
			value: {}
		});
	}

	on({name, callback}) {
		const HASH = Capsule.get({owner: this, key: EVENTS}),
			UNIQUE_CALLBACK = (...args) => callback(...args),
			OFF_CALLBACK = () => {
				const NAMED_EVENTS = HASH[name],
					REMOVED = deleteValue({array: NAMED_EVENTS, value: UNIQUE_CALLBACK}); 
				if(REMOVED) {
					if(!NAMED_EVENTS.length) {
						delete HASH[name];
					}
				}
				return REMOVED;
			};
		if(HASH[name]) {
			HASH[name].push(UNIQUE_CALLBACK);
		}
		else {
			HASH[name] = [UNIQUE_CALLBACK];
		}
		return OFF_CALLBACK;
	}

	one({name, callback}) {
		let offCallback;
		const UNIQUE_CALLBACK = (...args) => {
			offCallback();
			callback(...args);
		};
		offCallback = this.on({name, callback: UNIQUE_CALLBACK});
		return offCallback;
	}

	emit(name, ...args) {
		const HASH = Capsule.get({owner: this, key: EVENTS});
		if(HASH[name]) {
			HASH[name].forEach(callback => {
				callback(...args);
			});
		}
		return this;
	}
}