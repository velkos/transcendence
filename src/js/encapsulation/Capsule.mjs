const OWNERS = new WeakMap();

export const Capsule = {
	set({owner, key, value}) {
		if(OWNERS.has(owner)) {
			OWNERS.get(owner).set(key, value);
		}
		else {
			OWNERS.set(owner, new WeakMap().set(key, value));
		}
	},
	get({owner, key}) {
		if(OWNERS.has(owner)) {
			return OWNERS.get(owner).get(key);
		}
	},
	delete({owner, key}) {
		if(OWNERS.has(owner)) {
			return OWNERS.get(owner).delete(key);
		}
		return false;
	},
	createKey() {
		return Object.freeze(Object.create(null));
	}
};