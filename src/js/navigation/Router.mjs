import {deleteValue} from '../utilities/array/deleteValue.mjs';
import {Capsule} from '../encapsulation/Capsule.mjs';

const HOOKS = Capsule.createKey();

export class Router {
	constructor() {
		Capsule.set({
			owner: this,
			key: HOOKS,
			value: []
		});
	}

	route(callback) {
		const CALLBACKS = Capsule.get({owner: this, key: HOOKS}),
			UNIQUE_CALLBACK = (...args) => callback(...args),
			OFF_CALLBACK = () => {
				return deleteValue({
					array: CALLBACKS,
					value: UNIQUE_CALLBACK
				});
			};
		CALLBACKS.push(UNIQUE_CALLBACK);
		return OFF_CALLBACK;
	}

	serve(...args) {
		let i = 0;
		const ARGUMENTS = [...args],
			CALLBACKS = Capsule.get({owner: this, key: HOOKS}),
			NEXT = () => {
				if(i < CALLBACKS.length) {
					CALLBACKS[i++](...ARGUMENTS);
				}
				else {
					throw new Error('Unhandled route.');
				}
			};
		ARGUMENTS.unshift(NEXT);
		NEXT();
		return this;
	}
};