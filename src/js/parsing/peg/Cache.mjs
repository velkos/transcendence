const RULES = Symbol('rules');

export class Cache {
	constructor() {
		this[RULES] = new Map();
	}

	set({rule, offset, result}) {
		const MAP = this[RULES];
		if(MAP.has(rule)) {
			const RULE = MAP.get(rule);
			RULE[offset] = result;
		}
		else {
			MAP.set(
				rule,
				{
					[offset]: result
				}
			);
		}
		return this;
	}

	has({rule, offset}) {
		const RULE = this[RULES].get(rule);
		return !!(RULE && RULE.hasOwnProperty(offset));
	}

	get({rule, offset}) {
		const RULE = this[RULES].get(rule);
		if(RULE) {
			return RULE[offset];
		}
	}
}