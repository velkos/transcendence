export class Context {
	constructor({cursor, cache}) {
		Object.defineProperties(
			this,
			{
				cursor: {
					value: cursor
				},
				cache: {
					value: cache
				}
			}
		);
	}

	clone() {
		return new Context({
			cursor: this.cursor.clone(),
			cache: this.cache
		});
	}
};