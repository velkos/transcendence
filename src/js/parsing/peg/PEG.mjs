import {Cursor} from './input/Cursor.mjs';
import {Cache} from './Cache.mjs';
import {Context} from './Context.mjs';
import {String as StringParser} from './parser/String.mjs';
import {CharacterPredicate} from './parser/CharacterPredicate.mjs';
import {Sequence} from './parser/combinator/Sequence.mjs';
import {Choice} from './parser/combinator/Choice.mjs';
import {And} from './parser/predicate/And.mjs';
import {Not} from './parser/predicate/Not.mjs';
import {Range} from './parser/flow/Range.mjs';
import {CACHE_PARSE} from './interface/Cacher.mjs';

export {optional} from './parser/flow/optional.mjs';
export {zeroOrMore} from './parser/flow/zeroOrMore.mjs';
export {oneOrMore} from './parser/flow/oneOrMore.mjs';
export {digit} from './parser/preset/digit.mjs';
export {letter} from './parser/preset/letter.mjs';
export {printable} from './parser/preset/printable.mjs';
export {whitespace} from './parser/preset/whitespace.mjs';
export {any} from './parser/preset/any.mjs';
export const 
	string = string => {
		return StringParser.create(string);
	},
	characterPredicate = predicate => {
		return CharacterPredicate.create(predicate);
	},
	sequence = (...parsers) => {
		return Sequence.create(...parsers);
	},
	choice = (...parsers) => {
		return Choice.create(...parsers);
	},
	and = parser => {
		return And.create(parser);
	},
	not = parser => {
		return Not.create(parser);
	},
	/*Created in two steps for clear syntax.*/
	range = ({from, to}) => {
		return parser => {
			return Range.create({from, to, parser});
		}
	},
	parse = ({text, parser, cached = true}) => {
		return parser[CACHE_PARSE](
			new Context({
				cursor: new Cursor({text}),
				cache: cached ? new Cache() : null
			})
		);
	};