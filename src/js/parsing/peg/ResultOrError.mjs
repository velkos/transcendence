import {deepFreeze} from '../../utilities/object/deepFreeze.mjs';

export class ResultOrError {
	constructor({value, isError = false}) {
		Object.defineProperties(
			this,
			{
				value: {
					value: deepFreeze(value)
				},
				isError: {
					value: isError
				}
			}
		);
	}
};