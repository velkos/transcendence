import {Input} from './Input.mjs';

const OFFSET = Symbol('offset');

export class Cursor extends Input {
	constructor({text, offset = 0}) {
		super(text);
		this[OFFSET] = offset;
	}

	getOffset() {
		return this[OFFSET];
	}

	advance(n) {
		if(super.advance(n)) {
			this[OFFSET] += n;
			return true;
		}
		return false;
	}

	clone() {
		return new Cursor({
			text: this.getText(),
			offset: this[OFFSET]
		});
	}
};