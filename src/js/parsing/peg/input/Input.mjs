const TEXT = Symbol('text');

export class Input {
	constructor(text) {
		this[TEXT] = text;
	}

	getText() {
		return this[TEXT];
	}

	consumed() {
		return !this[TEXT].length;
	}

	advance(n) {
		const TXT = this[TEXT],
			LEN = TXT.length;
		if(n > 0 && n <= LEN) {
			this[TEXT] = TXT.slice(n);
			return true;
		}
		return false;
	}
};