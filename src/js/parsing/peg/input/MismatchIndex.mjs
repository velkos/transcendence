export const mismatchIndex = ({text, string}) => {
	const MIN = Math.min(string.length, text.length);
	let i = 0;
	for(; i < MIN; ++i) {
		if(text[i] !== string[i]) {
			return i;
		}
	}
	return i;
};