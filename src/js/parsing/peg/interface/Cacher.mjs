import {Wrapper} from './Wrapper.mjs';

export const CACHE_PARSE = Symbol('cacheParse'),
	PARSE = Symbol('parse');

export class Cacher extends Wrapper {
	[CACHE_PARSE](context) {
		const CACHE = context.cache;
		if(CACHE) {
			const CURSOR = context.cursor,
				CACHE_INFO = {
					rule: this,
					offset: CURSOR.getOffset()
				};
			if(CACHE.has(CACHE_INFO)) {
				return CACHE.get(CACHE_INFO);
			}
			else {
				const RESULT = CACHE_INFO.result = this[PARSE](context);
				CACHE.set(CACHE_INFO);
				return RESULT;
			}
		}
		else {
			return this[PARSE](context);
		}
	}
};