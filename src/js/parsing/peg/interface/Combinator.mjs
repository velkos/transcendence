import {Cacher} from './Cacher.mjs';
import {insert} from '../../../utilities/array/insert.mjs';

export const PARSERS = Symbol('parsers');

export class Combinator extends Cacher {
	constructor({[PARSERS]: parsers, ...rest}) {
		super(rest);
		if(!parsers || !parsers.length) {
			throw new Error(`Cannot create ${this.constructor.name} without parsers.`);
		}
		this[PARSERS] = parsers;
	}

	insert({parser, index}) {
		this[PARSERS].splice(index, 0, parser);
		return this;
	}
};