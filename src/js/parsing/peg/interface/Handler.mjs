import {Transformer, TRANSFORM} from './Transformer.mjs';
import {ResultOrError} from '../ResultOrError.mjs';

const HANDLE = Symbol('handle'),
	HANDLER = Symbol('handler');

export {HANDLE};

export class Handler extends Transformer {
	constructor({[HANDLER]: handler = null, ...rest} = {[HANDLER]: null}) {
		super(rest);
		this[HANDLER] = handler;
	}

	[HANDLE](error) {
		const H = this[HANDLER];
		return H ? H.call(this, error) : error;
	}

	handle(handler) {
		this[HANDLER] = handler;
		return this;
	}
};