const MAPPER = Symbol('mapper'),
	TRANSFORM = Symbol('transform');

export {TRANSFORM};

export class Transformer {
	constructor({[MAPPER]: mapper = null} = {[MAPPER]: null}) {
		this[MAPPER] = mapper;
	}

	map(mapper) {
		this[MAPPER] = mapper;
		return this;
	}

	clone() {
		return new this.constructor(this);
	}

	[TRANSFORM](match) {
		const M = this[MAPPER];
		return M ? M.call(this, match) : match;
	}
};