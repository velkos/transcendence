import {TRANSFORM} from './Transformer.mjs';
import {Handler, HANDLE} from './Handler.mjs';
import {ResultOrError} from '../ResultOrError.mjs';

export const SUCCESS = Symbol('success'),
	FAILURE = Symbol('failure');

export class Wrapper extends Handler {
	[SUCCESS]({match, length}) {
		return new ResultOrError({
			value: {
				match: this[TRANSFORM](match),
				length
			}
		});
	}

	[FAILURE](error) {
		return new ResultOrError({
			isError: true,
			value: this[HANDLE](error)
		});
	}
};