import {Cacher, PARSE} from '../interface/Cacher.mjs';
import {SUCCESS, FAILURE} from '../interface/Wrapper.mjs';

const PREDICATE = Symbol('predicate');

export class CharacterPredicate extends Cacher {
	constructor({[PREDICATE]: predicate, ...rest}) {
		super(rest);
		this[PREDICATE] = predicate;
	}

	[PARSE](context) {
		const CURSOR = context.cursor,
			CHAR = CURSOR.getText().charAt(0);
		return CHAR && this[PREDICATE](CHAR) ?
			this[SUCCESS]({
				match: CHAR,
				length: 1
			}) :
			this[FAILURE]({
				character: CHAR,
				index: CURSOR.getOffset()
			});
	}

	static create(predicate) {
		return new CharacterPredicate({[PREDICATE]: predicate});
	}
};