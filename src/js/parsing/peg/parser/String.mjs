import {Cacher, PARSE} from '../interface/Cacher.mjs';
import {SUCCESS, FAILURE} from '../interface/Wrapper.mjs';
import {mismatchIndex} from '../input/mismatchIndex.mjs';

const STRING = Symbol('string');

export class String extends Cacher {
	constructor({[STRING]: string, ...rest}) {
		super(rest);
		this[STRING] = string;
	}

	[PARSE](context) {
		const CURSOR = context.cursor,
			TEXT = CURSOR.getText(),
			STR = this[STRING],
			LEN = STR.length,
			INDEX = mismatchIndex({text: TEXT, string: STR});
		return INDEX && INDEX >= LEN ?
			this[SUCCESS]({
				match: STR,
				length: LEN
			}) :
			this[FAILURE]({
				expected: STR.charAt(INDEX),
				found: TEXT.charAt(INDEX),
				index: CURSOR.getOffset() + INDEX
			});
	}

	static create(string) {
		return new String({[STRING]: string});
	}
};