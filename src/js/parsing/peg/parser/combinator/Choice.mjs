import {Combinator, PARSERS} from '../../interface/Combinator.mjs';
import {CACHE_PARSE, PARSE} from '../../interface/Cacher.mjs';
import {SUCCESS, FAILURE} from '../../interface/Wrapper.mjs';

export class Choice extends Combinator {
	[PARSE](context) {
		const P = this[PARSERS],
			LAST = P.length - 1;
		let result,
			error;
		P.some((parser, index) => {
			const RESULT = parser[CACHE_PARSE](context.clone());
			if(RESULT.isError) {
				if(index === LAST) {
					error = {
						index,
						error: RESULT.value
					};
				}
				return false;
			}
			else {
				const {match: MATCH, length: LEN} = RESULT.value; 
				result = {
					match: {
						index,
						match: MATCH
					},
					length: LEN
				};
				return true;
			}
		});
		return result ?
			this[SUCCESS](result) :
			this[FAILURE](error)
	}

	static create(...parsers) {
		return new Choice({[PARSERS]: parsers});
	}
};