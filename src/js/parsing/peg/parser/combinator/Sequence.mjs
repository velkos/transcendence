import {Combinator, PARSERS} from '../../interface/Combinator.mjs';
import {PARSE, CACHE_PARSE} from '../../interface/Cacher.mjs';
import {SUCCESS, FAILURE} from '../../interface/Wrapper.mjs';

export class Sequence extends Combinator {
	[PARSE](context) {
		let error,
			len = 0;
		const MATCHES = [],
			CONTEXT = context.clone(),
			CURSOR = CONTEXT.cursor,
			PASS = this[PARSERS].every((parser, index) => {
				const RESULT = parser[CACHE_PARSE](CONTEXT);
				if(RESULT.isError) {
					error = {
						index,
						error: RESULT.value
					};
					return false;
				}
				else {
					const {match: MATCH, length: LEN} = RESULT.value;
					MATCHES.push(MATCH);
					CURSOR.advance(LEN);
					len += LEN;
					return true;
				}
			});
		return PASS ?
			this[SUCCESS]({
				match: MATCHES,
				length: len
			}) :
			this[FAILURE](error);
	}

	static create(...parsers) {
		return new Sequence({[PARSERS]: parsers});
	}
};