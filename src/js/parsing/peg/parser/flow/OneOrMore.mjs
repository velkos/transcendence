import {Range} from './Range.mjs';

export const oneOrMore = parser => {
	return Range.create({
		from: 0,
		to: Infinity,
		parser
	});
};