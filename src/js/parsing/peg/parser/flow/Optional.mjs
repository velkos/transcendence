import {Range} from './Range.mjs';

export const optional = parser => {
	return Range.create({
		from: 0,
		to: 1,
		parser
	});
};