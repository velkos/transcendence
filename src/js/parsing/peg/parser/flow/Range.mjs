import {Cacher, CACHE_PARSE, PARSE} from '../../interface/Cacher.mjs';
import {SUCCESS, FAILURE} from '../../interface/Wrapper.mjs';

const PARSER = Symbol('parser'),
	FROM = Symbol('from'),
	TO = Symbol('to');

export class Range extends Cacher {
	constructor({[PARSER]: parser, [FROM]: from, [TO]: to, ...rest}) {
		super(rest);
		this[PARSER] = parser;
		this[FROM] = from;
		this[TO] = to;
	}

	[PARSE](context) {
		const CONTEXT = context.clone(),
			CURSOR = CONTEXT.cursor,
			MAX = this[TO],
			MATCHES = [],
			P = this[PARSER];
		let i = 0,
			len = 0,
			error;
		while(i <= MAX) {
			const RESULT = P[CACHE_PARSE](CONTEXT);
			if(RESULT.isError) {
				error = RESULT.value;
				break;
			}
			else {
				const {match: MATCH, length: LEN} = RESULT.value;
				MATCHES.push(MATCH);
				CURSOR.advance(LEN);
				len += LEN;
				++i;
			}
		}

		return i >= this[FROM] ?
			this[SUCCESS]({
				match: MATCHES,
				length: len
			}) :
			this[FAILURE]({
				iteration: i,
				error
			});
	}

	static create({from, to, parser}) {
		return new Range({
			[FROM]: from,
			[TO]: to,
			[PARSER]: parser
		});
	}
};