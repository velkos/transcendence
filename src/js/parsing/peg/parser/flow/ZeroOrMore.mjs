import {Range} from './Range.mjs';

export const zeroOrMore = parser => {
	return Range.create({
		from: 0,
		to: Infinity,
		parser
	});
};