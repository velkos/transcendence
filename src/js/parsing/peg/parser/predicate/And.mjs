import {Cacher, CACHE_PARSE, PARSE} from '../../interface/Cacher.mjs';
import {SUCCESS, FAILURE} from '../../interface/Wrapper.mjs';

const PARSER = Symbol('parser');

export class And extends Cacher {
	constructor({[PARSER]: parser, ...rest}) {
		super(rest);
		this[PARSER] = parser;
	}

	[PARSE](context) {
		const CONTEXT = context.clone(),
			RESULT = this[PARSER][CACHE_PARSE](CONTEXT);
		return RESULT.isError ?
			this[FAILURE]({
				index: context.cursor.getOffset(),
				error: RESULT.value
			}) :
			this[SUCCESS]({...RESULT.value, length: 0});
	}

	static create(parser) {
		return new And({[PARSER]: parser});
	}
};