import {Cacher, CACHE_PARSE, PARSE} from '../../interface/Cacher.mjs';
import {SUCCESS, FAILURE} from '../../interface/Wrapper.mjs';

const PARSER = Symbol('parser'); 

export class Not extends Cacher {
	constructor({[PARSER]: parser, ...rest}) {
		super(rest);
		this[PARSER] = parser;
	}

	[PARSE](context) {
		const CONSUMED = context.cursor.consumed();
		return !CONSUMED && this[PARSER][CACHE_PARSE](context.clone()).isError ?
			this[SUCCESS]({
				match: undefined,
				length: 0
			}) :
			this[FAILURE]();
	}

	static create(parser) {
		return new Not({[PARSER]: parser});
	}
};