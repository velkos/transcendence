import {CharacterPredicate} from '../CharacterPredicate.mjs';

const IS_DIGIT = c => c >= '0' && c <= '9';
export const digit = () => CharacterPredicate.create(IS_DIGIT);