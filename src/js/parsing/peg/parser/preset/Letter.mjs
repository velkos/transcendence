import {CharacterPredicate} from '../CharacterPredicate.mjs';

const IS_LETTER = c => (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
export const letter = () => CharacterPredicate.create(IS_LETTER);