import {CharacterPredicate} from '../CharacterPredicate.mjs';

const IS_PRINTABLE = c => c >= '!' && c <= '~';
export const printable = () => CharacterPredicate.create(IS_PRINTABLE);