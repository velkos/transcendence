import {CharacterPredicate} from '../CharacterPredicate.mjs';

const WHITESPACE_REGEX = /\s/,
	IS_WHITESPACE = c => WHITESPACE_REGEX.test(c);
export const whitespace = () => CharacterPredicate.create(IS_WHITESPACE);