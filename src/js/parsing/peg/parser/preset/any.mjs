import {CharacterPredicate} from '../CharacterPredicate.mjs';

const ANY = () => true;
export const any = () => CharacterPredicate.create(ANY);