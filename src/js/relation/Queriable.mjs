import {Capsule} from '../encapsulation/Capsule.mjs';

const DATA = Capsule.createKey(),
	ASSERT_FUNCTION = (x, name) => {
		if(typeof x !== 'function') {
			throw new Error(`${name} is not a function.`);
		}
	};

export class Queriable {
	constructor(data = []) {
		if(!Array.isArray(data)) {
			throw new Error('Data not an array.');
		}
		Capsule.set({
			owner: this,
			key: DATA,
			value: data
		});
	}

	select(predicate) {
		ASSERT_FUNCTION(predicate, 'predicate');
		return new Queriable(
			Capsule.get({
				owner: this,
				key: DATA
			}).filter(value => predicate(value))
		);
	}

	insert(...records) {
		Capsule.get({owner: this, key: DATA}).push(...records);
		return this;
	}

	delete(predicate) {
		ASSERT_FUNCTION(predicate, 'predicate');
		Capsule.set({
			owner: this,
			key: DATA,
			value: Capsule.get({
				owner: this, key: DATA
			}).filter(value => !predicate(value))
		});
		return this;
	}

	alter({predicate, alterer}) {
		ASSERT_FUNCTION(predicate, 'predicate');
		ASSERT_FUNCTION(alterer, 'alterer');
		Capsule.get({
			owner: this,
			key: DATA
		}).forEach((value, index, array) => {
			if(predicate(value)) {
				array[index] = alterer(value);
			}
		});
		return this;
	}
}