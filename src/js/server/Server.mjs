import HTTP from 'http';
import PATH from 'path';
import {Router} from '../navigation/Router.mjs';

export class Server extends Router {
	constructor(config) {
		super();
		const SERVER = this.server = (config && config.server) || HTTP.createServer();
		SERVER.on('request', (request, response) => {
			this.serve(request, response);
		});
	}
}