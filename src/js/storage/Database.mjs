import {JSONFile} from './file/JSONFile.mjs';
import {Queriable} from '../data_structures/Queriable.mjs';

const FILE = Symbol('file');

export class Database extends Queriable {
	constructor({path, data}) {
		super();
		this[FILE] = new JSONFile({
			path,
			data
		});
	}

	persist() {
		return new Promise((resolve, reject) => {
			/*Preventing information leak when the promise gets resolved but preserving the error propagation when it gets rejected.*/
			this[FILE].write()
				.then(() => {
					resolve();
				})
				.catch(reject);
		});
	}

	restore() {
		return new Promise((resolve, reject) => {
			/*Preventing information leak when the promise gets resolved but preserving the error propagation when it gets rejected.*/
			this[FILE].read()
				.then(data => {
					this.setData(data);
					resolve();
				})
				.catch(reject);
		});
	}
};