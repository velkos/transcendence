import FS from 'fs';
import {Capsule} from '../../encapsulation/Capsule.mjs';
import {deepFreeze} from '../../utilities/object/deepFreeze.mjs';
import {InMemoryFile} from './InMemoryFile.mjs';
const FS_PROMISES = FS.promises;

const BUSY = Capsule.createKey(),
	DEF_R_ENC = null,
	DEF_R_FLAG = 'r',
	DEF_W_ENC = 'utf-8',
	DEF_W_MODE = 0o666,
	DEF_W_FLAG = 'w',
	SET_BUSY = ({owner, value}) => {
		Capsule.set({
			owner,
			key: BUSY,
			value: 
		})
	},
	GET_BUSY = owner => {
		return Capsule.get({
			owner,
			key: BUSY
		});
	},
	ASSERT_FREE = owner => {
		if(GET_BUSY(owner)) {
			throw new Error('Cannot perform operation while busy.');
		}
	};

export class File extends InMemoryFile {
	constructor(...args) {
		super(...args);
		SET_BUSY({owner: this, value: false});
	}

	isBusy() {
		return GET_BUSY(this);
	}

	setPath(path) {
		ASSERT_FREE(this);
		return super.setPath(path);
	}

	setData(data) {
		ASSERT_FREE(this);
		return super.setData(data);
	}

	read({encoding = DEF_R_ENC, flag = DEF_R_FLAG} = {encoding: DEF_R_ENC, flag: DEF_R_FLAG}) {
		ASSERT_FREE(this);
		SET_BUSY({owner: this, value: true});
		return FS_PROMISES.readFile(this.getPath(), encoding)
			.then(string => {
				const DATA = this.deserialize({string, options: arguments[0]});
				/*super.setData avoids availability assertion upgrade.*/
				super.setData(DATA);
				return DATA;
			})
			.finally(() => {
				SET_BUSY({owner: this, value: false});
			});
	}

	write({encoding = DEF_W_ENC, mode = DEF_W_MODE, flag = DEF_W_FLAG} = {encoding: DEF_W_ENC, mode: DEF_W_MODE, flag: DEF_W_FLAG}) {
		ASSERT_FREE(this);
		SET_BUSY({owner: this, value: true});
		const OPTIONS = deepFreeze(arguments[0]);
		return new Promise(resolve => {
			resolve(this.serialize({data: this.getData(), options: OPTIONS}));
		})
		.then(string => {
			return FS_PROMISES.writeFile(
					this.getPath(),
					string,
					OPTIONS
				)
				.then(() => string);
		})
		.finally(() => {
			SET_BUSY({owner: this, value: false});
		})
	}

	serialize({data, options}) {
		return String(data);
	}

	deserialize({string, options}) {
		return string;
	}
};