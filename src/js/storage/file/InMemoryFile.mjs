import {Capsule} from '../../encapsulation/Capsule.mjs';

const PATH = Capsule.createKey(),
	DATA = Capsule.createKey();

export class InMemoryFile {
	constructor({path = '', data = null} = {path: '', data: null}) {
		Capsule
		.set({
			owner: this,
			key: PATH,
			value: path
		})
		.set({
			owner: this,
			key: DATA,
			value: data
		});
	}

	getPath() {
		return Capsule.get({
			owner: this,
			key: PATH
		});
	}

	setPath(path) {
		Capsule.set({
			owner: this,
			key: PATH,
			value: path
		});
		return this;
	}

	getData() {
		return Capsule.get({
			owner: this,
			key: DATA,
		});
	}

	setData(data) {
		Capsule.set({
			owner: this,
			key: DATA,
			value: data
		});
		return this;
	}
};