import {File} from './File.mjs';

export class JSONFile extends File {
	serialize({data, options}) {
		return JSON.stringify(data);
	}

	deserialize({string, options}) {
		return JSON.parse(string);
	}
};