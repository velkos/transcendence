import {EventEmitter} from './communication/EventEmitter.mjs';

const EE = new EventEmitter();

EE.on({
	name: 'test',
	callback: (name, {title}) => {
		console.log('on', name, title);
	}
});

EE.one({
	name: 'test',
	callback: console.log.bind(console, 'one')
});

EE.emit('test', 'omg', {title: 'test subject'});
