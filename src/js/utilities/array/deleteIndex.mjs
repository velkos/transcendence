import {between} from '../math/between.mjs';

export const deleteIndex = ({array, index}) => {
	const LEN = array.length;
	if(LEN && between({min: 0, value: index, max: LEN})) {
		if(LEN > 1) {
			array.copyWithin(index, index + 1);
		}
		array.pop();
	}
};