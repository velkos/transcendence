import {deleteIndex} from './deleteIndex.mjs';

export const deleteValue = ({array, value, first = false}) => {
	return deleteIndex({
		array,
		index: first ? array.indexOf(value) : array.lastIndexOf(value)
	});
};