export const assert = (value, error) => {
	if(!value) throw new Error(error);
};