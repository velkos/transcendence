export const execute = ({source, context}) => {
	if(context) {
		return new Function(
			Object.keys(context),
			source
		)(...Object.values(context));	
	}
	else {
		return new Function(source)();
	}
}