const S4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1),
	guid =({prefix = '', postfix = ''} = {prefix: '', postfix: ''}) => {
		return `${prefix}${S4()}${S4()}-${S4()}-${S4()}-${S4()}-${S4()}${S4()}${S4()}${postfix}`;
	};

export {guid};