export const between = ({value, min, max}) => {
	const IS_FINITE = Number.isFinite;
	if(IS_FINITE(min), IS_FINITE(value) && IS_FINITE(max)) {
		return value >= min && value <= max;
	}
	return false;
};