export const randomInteger = ({min, max}) => {
	const MIN = Math.ceil(min),
		FLOOR = Math.floor,
		MAX = FLOOR(max);
	return FLOOR(Math.random() * (max - min + 1)) + min;
};