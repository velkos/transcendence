export const clear = object => {
	Object.keys(object).forEach(key => {
		delete object[key];
	});
	return object;
};