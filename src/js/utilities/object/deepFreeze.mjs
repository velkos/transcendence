import {isObject} from '../type/isObject.mjs';

export const deepFreeze = target => {
	if(isObject(target) || typeof target === 'function') {
		Object.freeze(target);
		Object.getOwnPropertyNames(target).forEach(key => {
			deepFreeze(target[key]);
		});
	}
	return target;
}