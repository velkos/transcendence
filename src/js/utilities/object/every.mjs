export const every = ({object, predicate}) => {
	return Object.keys(object).every(key => {
		return predicate({key, value: object[key], object});
	});
}