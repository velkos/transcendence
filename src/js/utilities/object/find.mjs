export const find = ({object, predicate}) => {
	const KEY = Object.keys(object).find(key => {
		return predicate({key, value: object[key], object});
	});
	if(KEY) {
		return {
			key: KEY,
			value: object[KEY]
		};
	}
}