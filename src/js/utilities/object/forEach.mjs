export const forEach = ({object, callback}) => {
	Object.keys(object).forEach(key => {
		callback({key, value: object[key], object});
	});
	return object;
};