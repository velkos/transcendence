const UNSAFE_CHARACTERS = /[\x00-\x2f\x3a-\x40\x5b-\x60\x7b-\xff]/g,
	ESCAPE_UNSAFE_CHARACTER = match => `&#${match.charCodeAt(0)};`,
	html = input => {
		UNSAFE_CHARACTERS.lastIndex = 0;
		return input.replace(UNSAFE_CHARACTERS, ESCAPE_UNSAFE_CHARACTER);
	};

export {html};