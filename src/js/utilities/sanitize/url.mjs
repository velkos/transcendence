import {URL} from 'url';

export const sanitizeURL = ({url, base}) => {
	const BASE = new URL(base),
		MAIN = new URL(url, BASE);
	return BASE.origin === MAIN.origin ? MAIN.href : BASE.href;
};