export const getType = x => {
	const TAG = Object.prototype.toString.call(x);
	return TAG.substring(8, TAG.length - 1);
};